use warnings;
use strict;

use Debian::Debhelper::Dh_Lib;

insert_after('dh_fixperms', 'dh_usrmove');

1;
